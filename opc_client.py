
import sys
sys.path.insert(0, "..")
import logging
import time
from sqlalchemy import create_engine, text


try:
    from IPython import embed
except ImportError:
    import code

    def embed():
        vars = globals()
        vars.update(locals())
        shell = code.InteractiveConsole(vars)
        shell.interact()


from opcua import Client
from opcua import ua


class SubHandler(object):

    """
    Subscription Handler. To receive events from server for a subscription
    data_change and event methods are called directly from receiving thread.
    Do not do expensive, slow or network operation there. Create another 
    thread if you need to do such a thing
    """

    def datachange_notification(self, node, val, data):
        print("Python: New data change event", node, val)

    def event_notification(self, event):
        print("Python: New event", event)


def get_db_conn(db_config):
    config_string = 'postgresql://{user}:{password}@{host}:{port}/{db_name}'. \
        format(user=db_config['postgres_user'],
               password=db_config['postgres_password'],
               host=db_config['postgres_host'],
               port=db_config['postgres_port'],
               db_name=db_config[
                   'postgres_db'])
    db = create_engine(config_string)
    return db

def write_point(node, db):
    timestamp = node.get_data_value().SourceTimestamp
    print(timestamp)
    value = node.get_value()
    tag = str(node).split('.')[-1][:-2]
    if isinstance(value, bool):
        b_value = value
        value = None
    else:
        b_value = None


    query = '''INSERT INTO raw (tag, value, b_value,timestamp)    
                    VALUES (:tag, :value, :b_value,:timestamp)
                    '''
    with db.engine.connect() as connection:
        try:
            status=connection.execute(text(query),
                                tag=tag,
                                value=value,
                                b_value=b_value,
                                timestamp=timestamp)
        except Exception as e:
            print(str(e))

def write_points(node):
    for node in node.get_children():
        try:
            write_point(node, conn)
        except Exception as e :
            print("exception")


if __name__ == "__main__":
    logging.basicConfig(level=logging.WARN)
    #logger = logging.getLogger("KeepAlive")
    #logger.setLevel(logging.DEBUG)

    client = Client("opc.tcp://127.0.0.1:4840/")
    pg_config = {
        'postgres_user': 'postgres',
        'postgres_password': 'postgres' ,
        'postgres_host': 'postgres.cshqsagecwhu.us-east-2.rds.amazonaws.com',
        'postgres_port':'5432',
        'postgres_db':'postgres'

    }
    conn = get_db_conn(pg_config)
    # client = Client("opc.tcp://admin@localhost:4840/freeopcua/server/") #connect using a user
    try:
        client.connect()

        # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects

        # Node objects have methods to read and write node attributes as well as browse or populate address space

        target_node = client.get_node("ns=4;s=|var|CODESYS Control for Raspberry Pi SL.Application.Study5_GVL")
        for i in range(4):

            write_points(target_node)
            time.sleep(10)


    finally:
        client.disconnect()


