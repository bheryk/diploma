from flask import Flask, render_template, request, redirect
from flask_restful import Resource, Api, reqparse
import time


import os
import calendar
import random 
app = Flask ( __name__ )
api = Api ( app )
from datetime import datetime 



class DrillDown ( Resource ) :
  parser = reqparse.RequestParser ()
  parser.add_argument ( 'type',
                        choices=(
                        'exception_app', 'exception_tx', 'apm_metrics_tx', 'apm_metrics_app', 'instance_metrics_app',
                        'infra_metrics_host', 'performance_app', 'performance_tx', 'search_tx'),
                        required=True )
  parser.add_argument ( 'host', required=False )


  
  def get ( self ) :
    value = random.random() *100
    timestamp = datetime.now().isoformat()

    return {'time': timestamp, 'value':value}
      
      

api.add_resource ( DrillDown, '/generate/' )

if __name__ == '__main__' :
  port = int ( os.environ.get ( 'PORT', 9000 ) )
  app.run ( host='0.0.0.0', port=port )
  # app.run ( debug=True )
