from flask import request, render_template, flash, redirect, url_for
from app import app
from app.forms import LoginForm
import bridge
from flask import jsonify 
from influxdb import InfluxDBClient
from sqlalchemy import create_engine, text
handler_path = '/home/ubuntu/projects/diploma/handler.py'

host='postgres.cshqsagecwhu.us-east-2.rds.amazonaws.com'
port=8086
user = 'docker'
password = 'P@ssword'
dbname = 'docker'
dbuser = 'docker'
dbuser_password = 'my_secret_password'


def update_threshold(tag,value) :
    query = "UPDATE  threshold  set value = :value where tag =:tag "

    
    with db.engine.connect() as connection:
        try:
            res=connection.execute(text(query), value=value, tag=tag)
        except Exception as e:
            print(str(e))
    

def get_db_conn():
    db_config = {
        'postgres_user': 'postgres',
        'postgres_password': 'postgres' ,
        'postgres_host': 'postgres.cshqsagecwhu.us-east-2.rds.amazonaws.com',
        'postgres_port':'5432',
        'postgres_db':'postgres'

    }
    config_string = 'postgresql://{user}:{password}@{host}:{port}/{db_name}'. \
        format(user=db_config['postgres_user'],
               password=db_config['postgres_password'],
               host=db_config['postgres_host'],
               port=db_config['postgres_port'],
               db_name=db_config[
                   'postgres_db'])
    db = create_engine(config_string)
    return db


db = get_db_conn()
client = InfluxDBClient(host, port, user, password, dbname)


@app.route('/submit', methods=['GET', 'POST'])
def submit():
    

    data = request.json
    
    
    
    for row in data:
        print(data[row]) 
        update_threshold(row, data[row])
    return(jsonify( {'response': 'success'}))
    
    form = LoginForm()
    if form.validate_on_submit():
        for f in form[0][0]:
            print(f) 
    
    return('success')

    global client
    global db

    form = LoginForm()
    if form.validate_on_submit():
        pass
    query = "SELECT  *  FROM threshold  "

    query = '''select * from threshold'''
    with db.engine.connect() as connection:
        try:
            res=connection.execute(text(query))
        except Exception as e:
            print(str(e))
    
    thresholds = []
    for row in res:
        update_threshold()
        thresholds.append({
            'tag':  row[1],
            'value': row[2]
        },)
        print(row)
    # tags = list(current_app.influxdb.query(act_severities_query.format(process_type=process_type,
    #                                           timestamp=timestamp)).get_points())

    # user = {'username': 'Miguel'}
    
    return render_template('index.html', title='Home', user=user, thresholds=thresholds)



@app.route('/')
@app.route('/index', methods=['GET', 'POST'])
def index():
    
    global client
    global db

    form = LoginForm()
    if form.validate_on_submit():
        pass
    query = "SELECT  *  FROM threshold  "

    query = '''select * from threshold'''
    with db.engine.connect() as connection:
        try:
            res=connection.execute(text(query))
        except Exception as e:
            print(str(e))
    
    thresholds = []
    for row in res:
        thresholds.append({
            'tag':  row[1],
            'value': row[2]
        },)
        print(row)
    # tags = list(current_app.influxdb.query(act_severities_query.format(process_type=process_type,
    #                                           timestamp=timestamp)).get_points())

    # user = {'username': 'Miguel'}
    
    return render_template('index.html', title='Home', user=user, thresholds=thresholds)


@app.route('/login', methods=['GET', 'POST'])
def login():
    print(app.config.__dict__)
    form = LoginForm()
    if form.validate_on_submit():
        # exec ()
        code = form.code.data 
        with open(handler_path, 'w+') as f:
            f.write(code)
        try: 
            res = bridge.main()
        except Exception as e:
            return str(e) 
        return res
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        
        return redirect(url_for('index'))
    return render_template('login.html',  title='Sign In', form=form)
