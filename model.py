
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
import os

DATABASE = {
    'NAME': os.environ.get('DB_NAME', 'postgres'),          # Database name
    'USER': os.environ.get('DB_USER', 'postgres'),          # PostgreSQL username
    'PASSWORD': os.environ.get('DB_PASSWORD', 'postgres'),  # PostgreSQL password
    'HOST': os.environ.get('DB_HOST', 'postgres.cshqsagecwhu.us-east-2.rds.amazonaws.com'),          # Database server
    'PORT': os.environ.get('DB_PORT', '5432'),      # Database port (leave blank for default)
}
DATABASE_URI = "postgres://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}".format(
    db_user=DATABASE['USER'],
    db_pass=DATABASE['PASSWORD'],
    db_host=DATABASE['HOST'],
    db_port=DATABASE['PORT'],
    db_name=DATABASE['NAME'],
)

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

class Raw(db.Model):
    __tablename__ = 'raw'
    id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.String(32), index=True, nullable=False)
    value = db.Column(db.Float(32), index=False, nullable=True)
    b_value = db.Column(db.Boolean, index=False, nullable=True)
    processed = db.Column(db.Float(32), index=False, nullable=True)
    timestamp = db.Column(db.DateTime, index=True, nullable=True)


class DataLoadWatermaks(db.Model):
    __tablename__ = 'watermarks'
    process_type = db.Column(db.String(32), nullable=False,primary_key=True)
    watermark = db.Column(db.DateTime, nullable=False)

class Thresholds(db.Model):
    __tablename__ = 'threshold'
    id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.String(32), nullable=False,primary_key=True)
    value = db.Column(db.Float(32), index=False, nullable=True)


if __name__ == '__main__':
    manager.run()

