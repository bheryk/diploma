import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    handler_path = '/home/ubuntu/projects/diploma/handler.py'
