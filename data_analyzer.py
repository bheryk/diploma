
import sys
sys.path.insert(0, "..")
import logging
import time
from datetime import datetime, timedelta
from sqlalchemy import create_engine, text
import sys
from slacker import Slacker
slack = Slacker('xoxb-362010617846-ztLUyRunsllcUyIAG7giHm6L')

try:
    from IPython import embed
except ImportError:
    import code

    def embed():
        vars = globals()
        vars.update(locals())
        shell = code.InteractiveConsole(vars)
        shell.interact()







def get_db_conn(db_config):
    config_string = 'postgresql://{user}:{password}@{host}:{port}/{db_name}'. \
        format(user=db_config['postgres_user'],
               password=db_config['postgres_password'],
               host=db_config['postgres_host'],
               port=db_config['postgres_port'],
               db_name=db_config[
                   'postgres_db'])
    db = create_engine(config_string)
    return db

def send_alarms(alarms):
    global slack
    print(alarms)    
    for alarm in alarms:
        slack.chat.post_message('#iot', alarm)


def compare(values,thresholds):
    alarms = []

    for key in values:
        if key in  thresholds:
            if values[key] > thresholds[key]:
                    alarms.append("Value {} with tag {} is higher than threshold value = {}".format(values[key],key, thresholds[key]))
    return alarms


def get_delaty_alarm(db):
    


    query = """SELECT max(timestamp) FROM raw """

    
    with db.engine.connect() as connection:
        try:
            res=connection.execute(text(query))
        except Exception as e:
            print(str(e))
    
    last_timestamp = datetime.now() 
    for row in res:
        last_timestamp = row[0]
    alarms = []
    if datetime.now() - last_timestamp > timedelta(minutes=3) 
        alarms.append('ALARM! Lost connection with Data Source. Last point: {} '.format(str(last_timestamp)))

    return thresholds


def get_thresholds(db):
    


    query = """SELECT * FROM threshold """

    
    with db.engine.connect() as connection:
        try:
            res=connection.execute(text(query))
        except Exception as e:
            print(str(e))
    
    thresholds = {}
    for row in res:
        thresholds[row[1]] = row[2] 
                   
    return thresholds

def get_values(db):
    
    query = """WITH last_rows AS ( SELECT m.*, ROW_NUMBER() OVER 
                (PARTITION BY tag ORDER BY timestamp DESC) AS rn
                FROM raw AS m where b_value is null )
                SELECT tag, value FROM last_rows WHERE rn = 1;"""

    
    with db.engine.connect() as connection:
        try:
            res=connection.execute(text(query))
        except Exception as e:
            print(str(e))
    
    values = {}
    for row in res:
        values[row[0]] = row[1] 
            
       
    return values

def write_point(node, db):
    timestamp = node.get_data_value().SourceTimestamp
    print(timestamp)
    value = node.get_value()
    tag = str(node).split('.')[-1][:-2]
    if isinstance(value, bool):
        b_value = value
        value = None
    else:
        b_value = None


    query = '''INSERT INTO raw (tag, value, b_value,timestamp)    
                    VALUES (:tag, :value, :b_value,:timestamp)
                    '''
    with db.engine.connect() as connection:
        try:
            status=connection.execute(text(query),
                                tag=tag,
                                value=value,
                                b_value=b_value,
                                timestamp=timestamp)
        except Exception as e:
            print(str(e))

def write_points(node):
    for node in node.get_children():
        try:
            write_point(node, conn)
        except Exception as e :
            print("exception")


if __name__ == "__main__":
    logging.basicConfig(level=logging.WARN)

    pg_config = {
        'postgres_user': 'postgres',
        'postgres_password': 'postgres' ,
        'postgres_host': 'postgres.cshqsagecwhu.us-east-2.rds.amazonaws.com',
        'postgres_port':'5432',
        'postgres_db':'postgres'

    }
    conn = get_db_conn(pg_config)
    values =  get_values(conn)
    thresholds = get_thresholds(conn)
    print(values)
    print(thresholds)
    
    alarms = compare(values, thresholds)
    alarms.extend(get_delaty_alarm)
    send_alarms(alarms)
    # client = Client("opc.tcp://admin@localhost:4840/freeopcua/server/") #connect using a user
