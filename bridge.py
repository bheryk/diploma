import requests
from influxdb import InfluxDBClient
from sqlalchemy import create_engine, text
import logging
import datetime
from logging import config
import importlib


logger = logging.getLogger(__name__)
default_log_conf = dict(
    version = 1,
    formatters = {
        'simple': {'format':
              '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'}
        },
    handlers = {
              'stderr': {'class': 'logging.StreamHandler',
              'formatter': 'simple',
              'level': logging.DEBUG,
              'stream' : 'ext://sys.stderr'}
        },
    root = {
        'handlers': ['stderr'],
        'level': logging.DEBUG,
        },
)

logging.config.dictConfig(default_log_conf)
logger = logging.getLogger('root')


def get_raw_data(engine, watermark ,process_type= 'all'):
    
    cmd = ''' SELECT  id, timestamp, value, b_value, tag  FROM raw
              WHERE timestamp > :watermark;
          '''
    with engine.connect() as connection:
        raw_data = connection.execute(text(cmd),
                            process_type=process_type,
                            watermark=watermark).fetchall()

    # if watermark does not exist - return interval 1 week from current date
    
    return raw_data


    

    
    
def get_watermark(postgres_engine, process_type):
    logger.debug('Loading watermark for process: ' + process_type)

    cmd = ''' SELECT  watermark FROM watermarks
              WHERE process_type = :process_type;
          '''
    with postgres_engine.engine.connect() as connection:
        watermark = connection.execute(text(cmd),
                            process_type=process_type).fetchone()

    # if watermark does not exist - return interval 1 week from current date
    if not watermark:
        watermark_value = datetime.datetime.now() - datetime.timedelta(days=40)
        logger.debug('Watermark value for process ' + process_type + ': ' + str(watermark_value))
        return watermark_value
    watermark_value = watermark[0]
    return watermark_value

def get_db_conn(db_config):
    config_string = 'postgresql://{user}:{password}@{host}:{port}/{db_name}'. \
        format(user=db_config['postgres_user'],
               password=db_config['postgres_password'],
               host=db_config['postgres_host'],
               port=db_config['postgres_port'],
               db_name=db_config[
                   'postgres_db'])
    db = create_engine(config_string)
    return db


def set_watermark(postgres_engine, process_type,watermark):
    logger.debug('Storing watermark for process {}. New value: '.format(
        process_type, watermark))

    cmd = ''' UPDATE watermarks
               SET  watermark = :watermark
               WHERE process_type = :process_type;
           '''
    with postgres_engine.engine.connect() as connection:
        connection.execute(text(cmd),
                           process_type=process_type,
                           watermark=watermark)


    
def write_points(points, engine):
    cmd = ''' UPDATE raw
               SET  processed = :value
               WHERE id = :id;
           '''

    with engine.connect() as connection:
        for point in points: 
            connection.execute(text(cmd),
                           value=point[1],
                           id=point[0])


def main():

    points = []
    pg_config = {
        'postgres_user': 'postgres',
        'postgres_password': 'postgres' ,
        'postgres_host': 'postgres.cshqsagecwhu.us-east-2.rds.amazonaws.com',
        'postgres_port':'5432',
        'postgres_db':'postgres'

    }
    conn = get_db_conn(pg_config)



    watermark = get_watermark(conn, 'all')
    logger.info(watermark)
    raw_data = get_raw_data(conn, watermark)
    # from handler import calculate
    # handler = importlib.import_module('handler')
    with open('handler.py', 'r') as f:
      exec(f)
      points = calculate(raw_data)
    logger.info(points)
    write_points(points, conn) 
    handler = None
    #set_watermark()
    return 'Succses'
    exit()

    response = requests.get('http://127.0.0.1:9000/generate/')
    resp_json = response.json()
    host='18.218.167.98'
    port=8086
    user = 'docker'
    password = 'P@ssword'
    dbname = 'docker'
    dbuser = 'docker'
    dbuser_password = 'my_secret_password'
    json_body = [
        {
            "measurement": "timeseries",
            "tags": {
                "host": "raspberyPi",
                
            },
            "time": resp_json['time'],
            "fields": {
                "random_value": resp_json['value']
            }
        }
    ]
    client = InfluxDBClient(host, port, user, password, dbname)
    # print("Write points: {0}".format(json_body))
    client.write_points(json_body)



if __name__=='__main__':
  main()

