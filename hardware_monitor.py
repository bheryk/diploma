import requests
from influxdb import InfluxDBClient
from sqlalchemy import create_engine, text
from datetime import datetime 
import logging
from logging import config
from copy import deepcopy
json_template = {
            "measurement": "timeseries",
            "tags": {
                "host": "raspberyPi",
                
            },
            "time": "",
            "fields": {

            }
        }
    

logger = logging.getLogger(__name__)
default_log_conf = dict(
    version = 1,
    formatters = {
        'simple': {'format':
              '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'}
        },
    handlers = {
              'stderr': {'class': 'logging.StreamHandler',
              'formatter': 'simple',
              'level': logging.DEBUG,
              'stream' : 'ext://sys.stderr'}
        },
    root = {
        'handlers': ['stderr'],
        'level': logging.DEBUG,
        },
)

logging.config.dictConfig(default_log_conf)
logger = logging.getLogger('root')


# Return CPU temperature as a character string                                      
def getCPUtemperature():
    res = os.popen('vcgencmd measure_temp').readline()
    json_body = deepcopy( json_template)
    json_body['time'] = datetime.now().isoformat()
    json_body['measurement'] = 'cpu_temp'
    json_body['fields']['value'] = float(res.replace("temp=","").replace("'C\n",""))
    return json_body

                                                               
def getRAMinfo():
    ''' Return RAM information (unit=Mb) in a list                                        
    Index 0: total RAM                                                                
    Index 1: used RAM                                                                 
    Index 2: free RAM  ''' 
    p = os.popen('free')
    i = 0
    json_body = deepcopy( json_template)
    while 1:
        i = i + 1
        line = p.readline()
        if i==2:
            json_body['time'] = datetime.now().isoformat()
            json_body['measurement'] = 'ram'
            values = line.split()[1:4]
            json_body['fields']['total'] = int(values[0]) /1024
            json_body['fields']['used'] = int(values[1]) /1024
            json_body['fields']['free'] = int(values[2]) /1024
            
            return json_body


# Return % of CPU used by user as a character string                                
def getCPUuse():
    json_body = deepcopy( json_template)
    json_body['time'] = datetime.now().isoformat()
    json_body['measurement'] = 'cpu_load'
    value = float( os.popen("top -n1 | awk '/Cpu\(s\):/ {print $2}'").readline().strip())
    json_body['fields']['value'] = value

    return json_body

# Return information about disk space as a list (unit included)                     
# Index 0: total disk space                                                         
# Index 1: used disk space                                                          
# Index 2: remaining disk space                                                     
# Index 3: percentage of disk used                                                  
def getDiskSpace():
    json_body = deepcopy( json_template)
    

    p = os.popen("df -h /")
    i = 0
    while 1:
        i = i +1
        line = p.readline()
        if i==2:
            
            json_body['time'] = datetime.now().isoformat()
            json_body['measurement'] = 'disk_space'
            values = line.split()[1:5]
            json_body['fields']['total'] = float(values[0])
            json_body['fields']['used'] = float(values[1])
            json_body['fields']['remaining'] = float(values[2])
            json_body['fields']['percentage'] = float(values[3])
            return json_body



    





def main():

    points = []
    pg_config = {
        'postgres_user': 'postgres',
        'postgres_password': 'postgres' ,
        'postgres_host': 'postgres.cshqsagecwhu.us-east-2.rds.amazonaws.com',
        'postgres_port':'5432',
        'postgres_db':'postgres'

    }
    # conn = get_db_conn(pg_config)



    # get_watermark(conn, 'all')
    # raw_data = get_raw_data(conn, watermark)
    # calculate_points(raw_data)
    # write_points (calculate_points(raw_data)) 
    # set_watermark()


    # response = requests.get('http://127.0.0.1:9000/generate/')
    # resp_json = response.json()
    host='ec2-18-188-51-150.us-east-2.compute.amazonaws.com'
    port=8086
    user = 'docker'
    password = 'P@ssword'
    dbname = 'docker'
    dbuser = 'docker'
    dbuser_password = 'my_secret_password'
    
    client = InfluxDBClient(host, port, user, password, dbname)
    # print("Write points: {0}".format(json_body))
    points = [] 
    for func in [getDiskSpace, getCPUtemperature, getCPUuse, getRAMinfo]:
        points.extend(func())

    print(points)
    return
    client.write_points(points)



if __name__=='__main__':
	main()

